package handlers

import (
	"net/http"

	"github.com/sirupsen/logrus"
)

type notFoundHandler struct {
	root string
}

func NotFoundHandler(root string) http.Handler {
	return &notFoundHandler{root}
}

// Gives index.html if requested not existing page.
// ReactJS should know how to handle 404 error
func (f *notFoundHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	logrus.WithFields(logrus.Fields{"location": "in handler.*notFoundHandler_ServeHTTP()"}).Info(r.URL.Path)
	http.ServeFile(w, r, f.root)
}
