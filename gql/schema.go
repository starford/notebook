package gql

import (
	"github.com/functionalfoundry/graphqlws"
	"github.com/graphql-go/graphql"
	"github.com/pkg/errors"
	"gitlab.com/starford/notebook/entity"
	"gitlab.com/starford/notebook/gql/resolver"
)

// GetSchema creates graphQL schema.
func GetSchema(noteBook []entity.Person) (*graphql.Schema, graphqlws.SubscriptionManager, error) {
	r := resolver.NewResolver(noteBook)
	schema, err := graphql.NewSchema(graphql.SchemaConfig{
		Query: graphql.NewObject(graphql.ObjectConfig{
			Name: "RootQuery",
			Fields: graphql.Fields{
				"noteBook": &graphql.Field{
					Type:        graphql.NewList(NoteBook),
					Description: "get all records",
					Resolve:     r.GetNoteBook,
				},
			},
		}),
		Mutation: graphql.NewObject(graphql.ObjectConfig{
			Name: "RootMutation",
			Fields: graphql.Fields{
				"editRecord": &graphql.Field{
					Type:        graphql.Boolean,
					Description: "edit record by id",
					Args: graphql.FieldConfigArgument{
						"id": &graphql.ArgumentConfig{
							Type: graphql.NewNonNull(graphql.Int),
						},
						"fName": &graphql.ArgumentConfig{
							Type: graphql.String,
						},
						"lName": &graphql.ArgumentConfig{
							Type: graphql.String,
						},
						"email": &graphql.ArgumentConfig{
							Type: graphql.String,
						},
						"phone": &graphql.ArgumentConfig{
							Type: graphql.String,
						},
					},
					Resolve: r.EditRecord,
				},
				"addRecord": &graphql.Field{
					Type:        graphql.Boolean,
					Description: "add a new person to the notebook",
					Args: graphql.FieldConfigArgument{
						"fName": &graphql.ArgumentConfig{
							Type: graphql.NewNonNull(graphql.String),
						},
						"lName": &graphql.ArgumentConfig{
							Type: graphql.String,
						},
						"email": &graphql.ArgumentConfig{
							Type: graphql.String,
						},
						"phone": &graphql.ArgumentConfig{
							Type: graphql.String,
						},
					},
					Resolve: r.AddRecord,
				},
				"removeRecord": &graphql.Field{
					Type:        graphql.Boolean,
					Description: "flush all records",
					Args: graphql.FieldConfigArgument{
						"id": &graphql.ArgumentConfig{
							Type: graphql.NewNonNull(graphql.Int),
						},
					},
					Resolve: r.RemoveRecord,
				},
			},
		}),
		Subscription: graphql.NewObject(graphql.ObjectConfig{
			Name: "RootSubscription",
			Fields: graphql.Fields{
				"onNoteBookUpdates": &graphql.Field{
					Type:        graphql.NewList(NoteBook),
					Description: "subscribe on updates in notebook",
					Resolve:     r.GetNoteBook,
				},
			},
		}),
	})
	subscriptionManager := graphqlws.NewSubscriptionManager(&schema)
	r.AddSchema(&schema)
	r.AddSubscriptionManager(subscriptionManager)
	return &schema, subscriptionManager, errors.Wrap(err, "in gql.GetSchema() on graphql.NewSchema()")
}
