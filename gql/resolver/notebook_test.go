package resolver

import (
	"reflect"
	"testing"

	"gitlab.com/starford/notebook/entity"

	"github.com/graphql-go/graphql"
	"gitlab.com/starford/notebook/mock"
)

func TestResolver_GetNoteBook(t *testing.T) {
	nt := mock.GetNoteBook()
	r := NewResolver(nt)
	p := graphql.ResolveParams{}
	res, err := r.GetNoteBook(p)
	if err != nil {
		t.Fatalf("in resolver.TestResolver_GetNoteBook() on r.GetNoteBook() err: %v", err)
	}
	if !reflect.DeepEqual(res, nt) {
		t.Fatal("in resolver.TestResolver_GetNoteBook() on reflect.DeepEqual() noteBooks are not equals")
	}
}

func TestSearchForARecord(t *testing.T) {
	nt := mock.GetNoteBook()
	rk, err := searchForARecord(3, nt)
	if err != nil {
		t.Fatalf("in resolver.TestSearchForARecord() on searchForARecord() err: %v", err)
	}
	expected := 2
	if rk != expected {
		t.Fatalf(`in resolver.TestSearchForARecord() on condition "rk != expected" expected to get: %v but got: %v`, expected, rk)
	}
}

func TestResolver_AddRecord(t *testing.T) {
	nt := mock.GetNoteBook()
	r := NewResolver(nt)
	p := graphql.ResolveParams{
		Args: map[string]interface{}{
			"fName": "John",
			"lName": "Doe",
			"email": "j.d@example.com",
			"phone": "+3(806)-965-654-85",
		},
	}
	_, err := r.AddRecord(p)
	if err != nil {
		t.Fatalf("in resolver.TestResolver_AddRecord() on r.AddRecord() err: %v", err)
	}
	expectedID := 6
	_, err = searchForARecord(expectedID, r.noteBook)
	if err != nil {
		t.Fatalf("in resolver.TestResolver_AddRecord() on searchForARecord() expected to get no any errors but got: %v", err)
	}
}

func TestResolver_RemoveRecord(t *testing.T) {
	nt := mock.GetNoteBook()
	r := NewResolver(nt)
	p := graphql.ResolveParams{
		Args: map[string]interface{}{
			"id": 3,
		},
	}
	_, err := r.RemoveRecord(p)
	if err != nil {
		t.Fatalf("in resolver.TestResolver_RemoveRecord() on r.RemoveRecord() err: %v", err)
	}
	expectedID := 3
	_, err = searchForARecord(expectedID, r.noteBook)
	if err == nil {
		t.Fatalf("in resolver.TestResolver_AddRecord() on searchForARecord() expected to get nil in err, but got: %v", err)
	}
}

func TestResolver_EditRecord(t *testing.T) {
	nt := mock.GetNoteBook()
	r := NewResolver(nt)
	p := graphql.ResolveParams{
		Args: map[string]interface{}{
			"id":    3,
			"fName": "John",
			"lName": "Doe",
			"email": "j.d@example.com",
			"phone": "+3(806)-965-654-85",
		},
	}
	_, err := r.EditRecord(p)
	if err != nil {
		t.Fatalf("in resolver.TestResolver_EditRecord() on r.EditRecord() err: %v", err)
	}
	expected := entity.Person{
		ID:    3,
		FName: "John",
		LName: "Doe",
		Email: "j.d@example.com",
		Phone: "+3(806)-965-654-85",
	}
	if !reflect.DeepEqual(r.noteBook[2], expected) {
		t.Fatal("in resolver.TestResolver_EditRecord() on reflect.DeepEqual() expected equal struct")
	}
}
