package resolver

import (
	"errors"
	"fmt"
	"reflect"

	"github.com/functionalfoundry/graphqlws"

	"gitlab.com/starford/notebook/entity"

	"github.com/graphql-go/graphql"
)

func (r *Resolver) GetNoteBook(p graphql.ResolveParams) (interface{}, error) {
	return r.noteBook, nil
}

func (r *Resolver) AddRecord(p graphql.ResolveParams) (interface{}, error) {
	lastRecord := len(r.noteBook) - 1
	id := r.noteBook[lastRecord].ID
	person := entity.Person{
		ID:    id + 1,
		FName: p.Args["fName"].(string),
		LName: p.Args["lName"].(string),
		Email: p.Args["email"].(string),
		Phone: p.Args["phone"].(string),
	}
	r.noteBook = append(r.noteBook, person)

	go func() {
		sendUpdatesToSubscriptions(r.schema, r.subscriptionManager)
	}()

	return true, nil
}

func (r *Resolver) RemoveRecord(p graphql.ResolveParams) (interface{}, error) {
	id := p.Args["id"].(int)
	personKey, err := searchForARecord(id, r.noteBook)
	if err != nil {
		return false, err
	}
	r.noteBook = append(r.noteBook[:personKey], r.noteBook[personKey+1:]...)

	go func() {
		sendUpdatesToSubscriptions(r.schema, r.subscriptionManager)
	}()

	return r.noteBook, nil
}

func (r *Resolver) EditRecord(p graphql.ResolveParams) (interface{}, error) {
	id := p.Args["id"].(int)
	personKey, err := searchForARecord(id, r.noteBook)
	if err != nil {
		return false, err
	}

	r.noteBook[personKey].FName = p.Args["fName"].(string)
	r.noteBook[personKey].LName = p.Args["lName"].(string)
	r.noteBook[personKey].Email = p.Args["email"].(string)
	r.noteBook[personKey].Phone = p.Args["phone"].(string)

	go func() {
		sendUpdatesToSubscriptions(r.schema, r.subscriptionManager)
	}()

	return true, nil
}

func searchForARecord(id int, noteBook []entity.Person) (recordKey int, err error) {
	if id <= 0 {
		return 0, errors.New("id param must not equal or lower zero")
	}

	var person entity.Person
	var personKey int

	for key, record := range noteBook {
		if record.ID == id {
			person = record
			personKey = key
		}
	}

	if reflect.DeepEqual(person, entity.Person{}) {
		err := fmt.Sprintf("record with id: %v, not found", id)
		return 0, errors.New(err)
	}
	return personKey, nil
}

func sendUpdatesToSubscriptions(schema *graphql.Schema, subscriptionManager graphqlws.SubscriptionManager) {
	for _, subs := range subscriptionManager.Subscriptions() {
		for _, sub := range subs {
			params := graphql.Params{
				Schema:         *schema,
				RequestString:  sub.Query,
				VariableValues: sub.Variables,
				OperationName:  sub.OperationName,
			}
			result := graphql.Do(params)

			data := graphqlws.DataMessagePayload{
				Data: result.Data,
				Errors: graphqlws.ErrorsFromGraphQLErrors(
					result.Errors,
				),
			}

			sub.SendData(&data)
		}
	}

}
