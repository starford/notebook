package resolver

import (
	"github.com/functionalfoundry/graphqlws"
	"github.com/graphql-go/graphql"
	"gitlab.com/starford/notebook/entity"
)

type Resolver struct {
	noteBook            []entity.Person
	subscriptionManager graphqlws.SubscriptionManager
	schema              *graphql.Schema
}

// NewResolver creates an instance of Resolver{}
func NewResolver(noteBook []entity.Person) *Resolver {
	return &Resolver{noteBook: noteBook}
}

// Add schema
func (r *Resolver) AddSchema(schema *graphql.Schema) {
	r.schema = schema
}

// Add subscription manager
func (r *Resolver) AddSubscriptionManager(sm graphqlws.SubscriptionManager) {
	r.subscriptionManager = sm
}
