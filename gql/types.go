package gql

import "github.com/graphql-go/graphql"

var NoteBook = graphql.NewObject(graphql.ObjectConfig{
	Name: "NoteBook",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: graphql.Int,
		},
		"fName": &graphql.Field{
			Type: graphql.String,
		},
		"lName": &graphql.Field{
			Type: graphql.String,
		},
		"email": &graphql.Field{
			Type: graphql.String,
		},
		"phone": &graphql.Field{
			Type: graphql.String,
		},
	},
})
