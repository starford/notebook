package entity

type Person struct {
	ID    int    `json:"id"`
	FName string `json:"fName"`
	LName string `json:"lName"`
	Email string `json:"email"`
	Phone string `json:"phone"`
}
