package main

import (
	"net/http"

	"github.com/functionalfoundry/graphqlws"

	"gitlab.com/starford/notebook/mock"

	"github.com/go-chi/chi"
	graphqlHandler "github.com/graphql-go/handler"
	"github.com/sirupsen/logrus"
	"gitlab.com/starford/notebook/gql"
	"gitlab.com/starford/notebook/handlers"
)

func main() {
	noteBook := mock.GetNoteBook()

	schema, subscriptionManager, err := gql.GetSchema(noteBook)
	if err != nil {
		logrus.Fatal("in main on graphql.GetSchema()", err)
	}

	graphqlWSHandler := graphqlws.NewHandler(graphqlws.HandlerConfig{
		SubscriptionManager: subscriptionManager,
	})

	r := chi.NewRouter()
	r.Use(handlers.DisableCORS)

	r.Group(func(r chi.Router) {
		h := graphqlHandler.New(&graphqlHandler.Config{
			Schema:   schema,
			Pretty:   true,
			GraphiQL: true,
		})
		r.Method(http.MethodPost, "/graphql", h)
	})
	r.Handle("/subscriptions", graphqlWSHandler)

	logrus.Fatal(http.ListenAndServe(":4000", r))
}
