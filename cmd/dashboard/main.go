package main

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/starford/notebook/handlers"
)

func main() {
	r := chi.NewRouter()

	publicDir := "/var/www/notebook"
	fs := http.FileServer(http.Dir(publicDir))

	r.Method(http.MethodGet, "/", fs)
	r.Method(http.MethodGet, "/static/{js|css|media}/*", fs)
	r.Method(http.MethodGet, "/img/*", fs)
	r.Method(http.MethodGet, "/favicon.ico", fs)
	r.Method(http.MethodGet, "/manifest.json", fs)

	r.NotFound(handlers.NotFoundHandler(publicDir).ServeHTTP)

	logrus.Fatal(http.ListenAndServe(":3000", r))
}
