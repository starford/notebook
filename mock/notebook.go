package mock

import "gitlab.com/starford/notebook/entity"

func GetNoteBook() []entity.Person {
	return []entity.Person{
		{
			ID:    1,
			FName: "Dean",
			LName: "Neal",
			Email: "dean.neal@example.com",
			Phone: "(129)-486-0195",
		}, {
			ID:    2,
			FName: "Tristan",
			LName: "Carroll",
			Email: "tristan.carroll@example.com",
			Phone: "(618)-475-4311",
		}, {
			ID:    3,
			FName: "Marilyn",
			LName: "Jennings",
			Email: "marilyn.jennings@example.com",
			Phone: "(886)-763-6372",
		}, {
			ID:    4,
			FName: "Maurice",
			LName: "Fleming",
			Email: "maurice.fleming@example.com",
			Phone: "(811)-225-4277",
		}, {
			ID:    5,
			FName: "Eli",
			LName: "Byrd",
			Email: "eli.byrd@example.com",
			Phone: "(218)-999-4011",
		},
	}
}
