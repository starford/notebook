<h1>
    Simple GraphQL app written on Go and React JS
</h1>

>>>
App could be executed in Docker containers, just run:

`$ make up`

Open in your browser:

http://localhost:3000
>>>

<div align="center">
	<img height="500" src="images/notebook-app.gif" alt="NoteBook App">
</div>


