import React, { Component } from 'react';
import NoteBookWrapper from './containers/NoteBookWrapper';

class App extends Component {
  render() {
    return <NoteBookWrapper/>;
  }
}

export default App;
