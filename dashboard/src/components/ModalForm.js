import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Label,
  Col,
  Input,
  FormText
} from 'reactstrap';
import PropTypes from 'prop-types';

class ModalForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fName: '',
      fNameError: '',
      lName: '',
      email: '',
      emailError: '',
      phone: '',
      pInfo: null,
    };
    this.validate = this.validate.bind(this);
    this.submit = this.submit.bind(this);
  }

  componentDidMount() {
    const {
      personInfo
    } = this.props;
    this.setState({pInfo: personInfo})
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const {
      personInfo
    } = nextProps;
    const {
      pInfo
    } = this.state;
    if (JSON.stringify(personInfo) !== JSON.stringify(pInfo)) {
      this.setState({
        fName: personInfo.fName,
        lName: personInfo.lName,
        email: personInfo.email,
        phone: personInfo.phone,
        pInfo: personInfo,
      })
    }
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  validate() {
    const {
      fName,
      email,
    } = this.state;
    let fNameError = '';
    if (fName === '') {
      fNameError = 'First name must not been an empty field';
    }
    let emailError = '';
    if (email !== '') {
      emailError = !email.includes('@') ? 'Email must contain @ symbol' : '';
    }

    if (fNameError || emailError) {
      this.setState({ fNameError, emailError });
      return false;
    }
    return true;
  };

  submit() {
    const {
      title,
      addRecord,
      editRecord,
      personInfo,
    } = this.props;
    const {
      fName, lName, email, phone
    } = this.state;
    if (title === 'Add new record') {
      addRecord(fName, lName, email, phone);
    } else {
      editRecord(personInfo.id, fName, lName, email, phone)
    }
  }

  render() {
    const {
      toggleModal,
      modalIsOpen,
      title,
      setPersonInfo,
    } = this.props;
    const {
      fName,
      fNameError,
      lName,
      email,
      emailError,
      phone,
    } = this.state;
    return (
      <Form>
        <Modal
          isOpen={modalIsOpen}
          toggle={toggleModal}
          onClosed={() => setPersonInfo(0, '', '', '', '')}
          size='lg'
        >
          <ModalHeader toggle={toggleModal}>{title}</ModalHeader>
          <ModalBody>
              <FormGroup row>
                <Label for="fName" sm={2}>*First name:</Label>
                <Col sm={10}>
                  <Input
                    type="text"
                    name="fName"
                    id="fName"
                    placeholder="First name..."
                    value={fName}
                    onChange={this.handleChange}
                  />
                  {
                    fNameError !== ''
                      ? <FormText className="text-warning">{fNameError}</FormText>
                      : null
                  }
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="lName" sm={2}>Last name:</Label>
                <Col sm={10}>
                  <Input
                    type="text"
                    name="lName"
                    id="lName"
                    placeholder="Last name..."
                    value={lName}
                    onChange={this.handleChange}
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="email" sm={2}>Email:</Label>
                <Col sm={10}>
                  <Input
                    type="email"
                    name="email"
                    id="email"
                    placeholder="Email..."
                    value={email}
                    onChange={this.handleChange}
                  />
                  {
                    emailError !== ''
                      ? <FormText>{emailError}</FormText>
                      : null
                  }
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="phone" sm={2}>Phone:</Label>
                <Col sm={10}>
                  <Input
                    type="email"
                    name="phone"
                    id="phone"
                    placeholder="Phone..."
                    value={phone}
                    onChange={this.handleChange}
                  />
                </Col>
              </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button
              color="success"
              onClick={() => {
                const isValid = this.validate();
                if (isValid) {
                  this.submit();
                  toggleModal()
                }
              }}
            >
              Submit
            </Button>
          </ModalFooter>
        </Modal>
      </Form>
    );
  }
}

ModalForm.propTypes = {
  personInfo: PropTypes.shape({}),
  addRecord: PropTypes.func,
  editRecord: PropTypes.func,
  setPersonInfo: PropTypes.func,
  toggleModal: PropTypes.func,
  modalIsOpen: PropTypes.bool,
};

export default ModalForm;
