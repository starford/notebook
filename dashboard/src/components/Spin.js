import React, { Component } from 'react';
import {Col, Container, Row, Spinner} from 'reactstrap';

class Spin extends Component {
  render() {
    return (
      <div className='d-flex vh-100'>
        <Container className='align-self-center'>
          <Row>
            <Col md={12}>
              <div className='d-flex justify-content-center mw-100'>
                <Spinner color="info" size='md' type="grow" />
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Spin;
