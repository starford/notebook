import gql from 'graphql-tag';

export const getCurrentModalState = gql`
  query modalIsOpen {
    modalIsOpen @client {
      isOpen
    }
  }
`;

export const toggleModal = gql`
  mutation toggleModal($isOpen: Boolean!) {
    toggleModal(isOpen: $isOpen) @client {
      isOpen
    }
  }
`;

export const getPersonInfo = gql`
  query personInfo {
    personInfo @client {
      id,
      fName,
      lName,
      email,
      phone
    }
  }
`;

export const setPersonInfo = gql`
  mutation setPersonInfo($id: Int, $fName: String, $lName: String, $email: String, $phone: String) {
    setPersonInfo(id: $id, fName: $fName, lName: $lName, email: $email, phone: $phone) @client
  }
`;
