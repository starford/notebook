import * as Q from './query';

export const resolvers = {
  Mutation: {
    toggleModal: (_, { isOpen }, { cache }) => {
      const query = Q.getCurrentModalState;
      const previous = cache.readQuery({ query });
      const data = {
        modalIsOpen: {
          ...previous.modalIsOpen,
          isOpen,
        },
      };
      cache.writeQuery({ query, data });
      return null;
    },
    setPersonInfo: (_, { id, fName, lName, email, phone }, { cache }) => {
      const query = Q.getPersonInfo;
      const previous = cache.readQuery({ query });
      const data = {
        personInfo: {
          ...previous.personInfo,
          id,
          fName,
          lName,
          email,
          phone,
        },
      };
      cache.writeQuery({ query, data });
      return null;
    },
  },
};
