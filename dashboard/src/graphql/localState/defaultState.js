export const defaultState = {
  modalIsOpen: {
    __typename: 'modalIsOpen',
    isOpen: false,
  },
  personInfo: {
    __typename: 'personInfo',
    id: 0,
    fName: '',
    lName: '',
    email: '',
    phone: '',
  },
};
