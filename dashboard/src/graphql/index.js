import gql from 'graphql-tag';

export const GetNoteBook = gql`
query noteBook{
  noteBook{
    email
    fName
    id
    lName
    phone
  }
}
`;

export const AddRecord = gql`
mutation addRecord($fName: String!, $lName: String, $email: String, $phone: String){
  addRecord(fName: $fName, lName: $lName, email: $email, phone: $phone)
}
`;

export const EditRecord = gql`
mutation editRecord($id: Int!, $fName: String!, $lName: String, $email: String, $phone: String){
  editRecord(id: $id, fName: $fName, lName: $lName, email: $email, phone: $phone)
}
`;

export const RemoveRecord = gql`
mutation removeRecord($id: Int!){
  removeRecord(id: $id)
}
`;

export const SubscribeOnNoteBookUpdates = gql`
subscription onNoteBookUpdates{
  onNoteBookUpdates{
    email
    fName
    id
    lName
    phone
  }
}
`;
