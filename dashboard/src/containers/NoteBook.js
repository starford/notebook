import React, { Component } from 'react';
import ReactTable from 'react-table';
import getColumns from './helpers/getcolumns';
import PropTypes from 'prop-types';
import 'react-table/react-table.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faPencilAlt, faTimes } from '@fortawesome/free-solid-svg-icons';

library.add(faPencilAlt, faTimes);

class NoteBook extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
      editModalIsOpen: false,
      userInfo: null,
      columns: [],
    };
  }

  componentDidMount() {
    const {
      removeRecord,
      toggleModal,
      toggleModalTitle,
      setPersonInfo,
      subscribeOnNoteBookUpdates,
    } = this.props;
    subscribeOnNoteBookUpdates();
    const columns = getColumns(removeRecord, toggleModal, toggleModalTitle, setPersonInfo);
    this.setState({ columns });
  }

  render() {
    const {
      noteBook
    } = this.props;
    const {
      columns
    } = this.state;
    return (
      <ReactTable
        data={noteBook}
        columns={columns}
        filterable
        defaultFilterMethod={(filter, row) => String(row[filter.id]) === filter.value}
        defaultPageSize={10}
      />
    );
  }
}

NoteBook.propTypes = {
  noteBook: PropTypes.array,
  removeRecord: PropTypes.func,
  setPersonInfo: PropTypes.func,
  toggleModal: PropTypes.func,
  toggleModalTitle: PropTypes.func,
  subscribeOnNoteBookUpdates: PropTypes.func,
};

export default NoteBook;
