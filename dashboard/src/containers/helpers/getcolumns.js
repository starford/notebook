import React from 'react';
import matchSorter from 'match-sorter';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export default function (removeRecord, toggleModal, toggleModalTitle, setPersonInfo) {
  return [
    {
      columns: [
        {
          Header: 'First name',
          accessor: 'fName',
          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ['fName'] }),
          filterAll: true,
        },
      ],
    }, {
      columns: [
        {
          Header: 'Last name',
          accessor: 'lName',
          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ['lName'] }),
          filterAll: true,
        },
      ],
    }, {
      columns: [
        {
          Header: 'Email',
          accessor: 'email',
          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ['email'] }),
          filterAll: true,
        },
      ],
    }, {
      columns: [
        {
          Header: 'Phone',
          accessor: 'phone',
          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ['phone'] }),
          filterAll: true,
        },
      ],
    }, {
      accessor: 'edit-delete',
      width: 100,
      Cell: row => (
        <>
          <button
            className="btn-transparent edit-btn btn-sm ml-3 mr-4 p-0"
            onClick={() => {
              toggleModalTitle();
              setPersonInfo(
                row.original.id,
                row.original.fName,
                row.original.lName,
                row.original.email,
                row.original.phone,
              );
              toggleModal();
            }}
          >
            <FontAwesomeIcon icon="pencil-alt"/>
          </button>
          <button
            className="btn-transparent delete-btn btn-sm ml-2 p-0"
            onClick={() => removeRecord(row.original.id)}
          >
            <FontAwesomeIcon icon="times"/>
          </button>
        </>
      ),
      filterable: false,
      sortable: false,
    }
  ];
}
