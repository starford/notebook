import React, {Component} from 'react';
import { compose, graphql } from 'react-apollo';
import * as QS from '../graphql/localState/query';
import * as Q from '../graphql';
import PropTypes from 'prop-types';
import NoteBook from './NoteBook';
import { Container, Row, Col, Button } from 'reactstrap';
import Spin from '../components/Spin';
import ModalForm from "../components/ModalForm";

class NoteBookWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalTitle: ''
    };
    this.toggleModalTitle = this.toggleModalTitle.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
  }

  toggleModalTitle(isRegisterForm) {
    const addTitle = 'Add new record';
    const editTitle = 'Edit record';
    this.setState({
      modalTitle: isRegisterForm ? addTitle : editTitle,
    })
  }

  toggleModal() {
    const {
      toggleModal,
      modalIsOpen,
    } = this.props;
    toggleModal(!modalIsOpen.isOpen)
  }

  render() {
    const {
      loading,
      error,
      noteBook,
      modalIsOpen,
      removeRecord,
      setPersonInfo,
      personInfo,
      addRecord,
      editRecord,
      subscribeToMore,
    } = this.props;
    const {
      modalTitle,
    } = this.state;
    if (loading) return <Spin />;
    if (error) return <p>{error.message}</p>;
    return (
      <div className='d-flex vh-100'>
        <Container className='align-self-center'>
          <Row>
            <Col md={12}>
              <div className='mb-3 d-flex justify-content-end'>
                <Button
                  className='btn-success'
                  onClick={() => {
                    this.toggleModalTitle(true);
                    this.toggleModal()
                  }}
                >
                  Add record
                </Button>
              </div>
            </Col>
            <Col md={12}>
              <NoteBook
                noteBook={noteBook}
                removeRecord={removeRecord}
                toggleModalTitle={this.toggleModalTitle}
                toggleModal={this.toggleModal}
                setPersonInfo={setPersonInfo}
                subscribeOnNoteBookUpdates={() => {
                  subscribeToMore({
                    document: Q.SubscribeOnNoteBookUpdates,
                    updateQuery: (prev, { subscriptionData }) => {
                      if (!subscriptionData.data) return prev;
                      return {
                        noteBook: subscriptionData.data.onNoteBookUpdates,
                      };
                    },
                  });
                }}

              />
            </Col>
          </Row>
          <ModalForm
            modalIsOpen={modalIsOpen.isOpen}
            toggleModal={this.toggleModal}
            title={modalTitle}
            personInfo={personInfo}
            setPersonInfo={setPersonInfo}
            addRecord={addRecord}
            editRecord={editRecord}
          />
        </Container>
      </div>
    );
  }
}

NoteBookWrapper.propTypes = {
  modalIsOpen: PropTypes.shape({}),
  personInfo: PropTypes.shape({}),
  noteBook: PropTypes.array,
  addRecord: PropTypes.func,
  editRecord: PropTypes.func,
  removeRecord: PropTypes.func,
  setPersonInfo: PropTypes.func,
  toggleModal: PropTypes.func,
  loading: PropTypes.bool,
  error: PropTypes.any,
  subscribeToMore: PropTypes.any,
};

export default compose(
  graphql(Q.GetNoteBook, {
    props: ({ data }) => {
      const {
        loading, error, noteBook, subscribeToMore,
      } = data;
      return {
        loading, error, noteBook, subscribeToMore,
      };
    },
    options: () => ({
      fetchPolicy: 'network-first',
    }),
  }),
  graphql(QS.getCurrentModalState, {
    props: ({ data }) => {
      const {
        modalIsOpen,
      } = data;
      return {
        modalIsOpen,
      };
    },
  }),
  graphql(QS.getPersonInfo, {
    props: ({ data }) => {
      const {
        personInfo,
      } = data;
      return {
        personInfo,
      };
    },
  }),
  graphql(Q.AddRecord, {
    props: ({mutate}) => ({
      addRecord: (fName, lName, email, phone) => mutate({variables: {fName, lName, email, phone}}),
    }),
  }),
  graphql(Q.EditRecord, {
    props: ({mutate}) => ({
      editRecord: (id, fName, lName, email, phone) => mutate({variables: {id, fName, lName, email, phone}}),
    }),
  }),
  graphql(Q.RemoveRecord, {
    props: ({mutate}) => ({
      removeRecord: (id) => mutate({variables: {id}}),
    }),
  }),
  graphql(QS.setPersonInfo, {
    props: ({mutate}) => ({
      setPersonInfo: (id, fName, lName, email, phone) => mutate({variables: {id, fName, lName, email, phone}}),
    }),
  }),
  graphql(QS.toggleModal, {
    props: ({mutate}) => ({
      toggleModal: (isOpen) => mutate({variables: {isOpen}}),
    }),
  }),
)(NoteBookWrapper);
