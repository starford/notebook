PKGS := $(shell go list ./... | grep -v /vendor)
ROOTDIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
IMGV ?= latest
test:
		go test -race -cover $(PKGS)

image-%:
		docker build -t registry.gitlab.com/starford/notebook/$*:$(IMGV) $(ROOTDIR)var/$*/
		docker push registry.gitlab.com/starford/notebook/$*:$(IMGV)

dashboard-build:
		cd $(ROOTDIR)dashboard/ && yarn build
		rsync -av --exclude='*.map' $(ROOTDIR)dashboard/build/* $(ROOTDIR)var/dashboard/notebook/
		rm -rf $(ROOTDIR)dashboard/build
		go build -race -o $(ROOTDIR)var/dashboard/bin/dashboard $(ROOTDIR)cmd/dashboard/main.go
		$(MAKE) image-dashboard

graphql-build:
		go build -race -o $(ROOTDIR)var/graphql/bin/graphql $(ROOTDIR)cmd/graphql/main.go
		$(MAKE) image-graphql

build:
		$(MAKE) graphql-build
		$(MAKE) dashboard-build

up:
		docker-compose up -d

stop:
		docker-compose stop

down:
		docker-compose down -v
